<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace think\template\taglib\gzpcf;
use think\facade\Db;
use think\facade\Page;
use think\facade\Request;
use app\home\logic\FieldLogic;
use app\common\model\Arctype;
use app\common\model\ChannelType;

class TagList extends Base
{
    public $tid = '';
    public $fieldLogic;
    public $url_screen_var;
    
    //初始化
    protected function _initialize()
    {
        parent::_initialize();
        $this->pcfglobal = get_global();
        $this->fieldLogic = new FieldLogic();
        $this->Arctype = new Arctype();
        $this->ChannelType = new ChannelType();
        $this->tid = input('param.tid/s', '');
        // 应用于文档列表
        $aid = input('param.aid/d', 0);
        if ($aid > 0) {
            $this->tid = Db::name('archives')->where('aid', $aid)->value('typeid');
        }
        // typeid|tid为目录名称的情况下
        $this->tid = $this->getTrueTypeid($this->tid);
        // 定义筛选标识
        $this->url_screen_var = $this->pcfglobal['url_screen_var'];
    }

    // 获取分页列表
    public function getList($param =array(),$pagesize = 10,$orderby = '',$addfields = '',$orderway = '',$thumb = '',$arcrank = '')
    {
        $ctl_name_tmp = strtolower(Request::controller());
        empty($orderway) && $orderway = 'desc';
        // 自定义字段筛选
        $url_screen_var = input('param.'.$this->url_screen_var.'/d');
        if (1 == $url_screen_var) {
           //return $this->GetFieldScreeningList($param,$pagesize, $orderby, $addfields, $orderway, $thumb);
        }
        // 搜索
        if (in_array($ctl_name_tmp, array('search'))) {
           return $this->getSearchList($pagesize, $orderby, $addfields, $orderway, $thumb);
        }
        $result = false;
        $channeltype = ("" != $param['channel'] && is_numeric($param['channel'])) ? intval($param['channel']) : '';
        $param['typeid'] = !empty($param['typeid']) ? $param['typeid'] : $this->tid;
        if (!empty($param['typeid'])) {
            if (!preg_match('/^\d+([\d\,]*)$/i', $param['typeid'])) {
                echo '标签list报错：typeid属性值语法错误，请正确填写栏目ID。';
                return false;
            }
            // 过滤typeid中含有空值的栏目ID
            $typeidArr_tmp = explode(',', $param['typeid']);
            $typeidArr_tmp = array_unique($typeidArr_tmp);
            foreach($typeidArr_tmp as $k => $v){   
                if (empty($v)) unset($typeidArr_tmp[$k]);  
            }
            $param['typeid'] = implode(',', $typeidArr_tmp);
        }
        $typeid = $param['typeid'];
        // 不指定模型ID、栏目ID，默认显示所有可以发布文档的模型ID下的文档
        if (("" === $channeltype && empty($typeid)) || 0 === $channeltype) {
            $allow_release_channel = $this->pcfglobal['allow_release_channel'];
            $channeltype = $param['channel'] = implode(',', $allow_release_channel);
        }
        // 如果指定了频道ID，则频道下的所有文档都展示
        if (!empty($channeltype)) { 
            // 优先展示模型下的文章
            unset($param['typeid']);
        } 
        // 其次展示栏目下的文章
        elseif (!empty($typeid)) { 
            $typeidArr = explode(',', $typeid);
            if (count($typeidArr) == 1) {
                $typeid = intval($typeid);
                $channel_info = Db::name('Arctype')->field('id,current_channel')->where('id',$typeid)->find();
                if (empty($channel_info)) {
                    echo '标签list报错：指定属性 typeid 的栏目ID不存在。';
                    return false;
                }
                $channeltype = !empty($channel_info) ? $channel_info["current_channel"] : '';
                // 获取当前栏目下的同模型所有子孙栏目
                $arctype_list = $this->Arctype->getHasChildren($channel_info['id']);
                foreach ($arctype_list as $key => $val) {
                    if ($channeltype != $val['current_channel']) {
                        unset($arctype_list[$key]);
                    }
                }
                $typeids = get_arr_column($arctype_list, "id");
                !in_array($typeid, $typeids) && $typeids[] = $typeid;
                $typeid = implode(",", $typeids);
            } 
            $param['channel'] = $channeltype;
        } 
        else { 
            // 再次展示控制器对应的模型文章
            $controller_name = Request::controller();
            $channeltype_info = $this->ChannelType->getInfoByWhere(array('ctl_name'=>$controller_name), 'id');
            if (!empty($channeltype_info)) {
                $channeltype = $channeltype_info['id'];
                $param['channel'] = $channeltype;
            }
        }
        // 查询条件
        $condition = array();
        foreach (array('keywords','typeid','notypeid','flag','noflag','channel') as $key) {
            if (isset($param[$key]) && $param[$key] !== '') {
                if ($key == 'keywords') {
                    array_push($condition, "a.title LIKE '%{$param[$key]}%'");
                } elseif ($key == 'channel') {
                    array_push($condition, "a.channel IN ({$channeltype})");
                } elseif ($key == 'typeid') {
                    array_push($condition, "a.typeid IN ({$typeid})");
                } elseif ($key == 'notypeid') {
                    $param[$key] = str_replace('，', ',', $param[$key]);
                    array_push($condition, "a.typeid NOT IN (".$param[$key].")");
                } elseif ($key == 'flag') {
                    $flag_arr = explode(",", $param[$key]);
                    $where_or_flag = array();
                    foreach ($flag_arr as $k2 => $v2) {
                        if ($v2 == "c") {
                            array_push($where_or_flag, "a.is_recom = 1");
                        } elseif ($v2 == "h") {
                            array_push($where_or_flag, "a.is_head = 1");
                        } elseif ($v2 == "a") {
                            array_push($where_or_flag, "a.is_special = 1");
                        } elseif ($v2 == "j") {
                            array_push($where_or_flag, "a.is_jump = 1");
                        } elseif ($v2 == "p") {
                            array_push($where_or_flag, "a.is_litpic = 1");
                        } elseif ($v2 == "b") {
                            array_push($where_or_flag, "a.is_b = 1");
                        }
                    }
                    if (!empty($where_or_flag)) {
                        $where_flag_str = " (".implode(" OR ", $where_or_flag).") ";
                        array_push($condition, $where_flag_str);
                    } 
                } elseif ($key == 'noflag') {
                    $flag_arr = explode(",", $param[$key]);
                    $where_or_flag = array();
                    foreach ($flag_arr as $nk2 => $nv2) {
                        if ($nv2 == "c") {
                            array_push($where_or_flag, "a.is_recom <> 1");
                        } elseif ($nv2 == "h") {
                            array_push($where_or_flag, "a.is_head <> 1");
                        } elseif ($nv2 == "a") {
                            array_push($where_or_flag, "a.is_special <> 1");
                        } elseif ($nv2 == "j") {
                            array_push($where_or_flag, "a.is_jump <> 1");
                        } elseif ($nv2 == "p") {
                            array_push($where_or_flag, "a.is_litpic <> 1");
                        } elseif ($nv2 == "b") {
                            array_push($where_or_flag, "a.is_b <> 1");
                        }
                    }
                    if (!empty($where_or_flag)) {
                        $where_flag_str = " (".implode(" OR ", $where_or_flag).") ";
                        array_push($condition, $where_flag_str);
                    }
                } else {
                    array_push($condition, "a.{$key} = '".$param[$key]."'");
                }
            }
        }
        array_push($condition, "a.arcrank > -1");
        array_push($condition, "a.status = 1");
        array_push($condition, "a.is_del = 0"); // 回收站功能
        $where_str = "";
        if (0 < count($condition)) {
            $where_str = implode(" AND ", $condition);
        }
        // 给排序字段加上表别名
        $orderby = getOrderBy($orderby,$orderway);
        // 是否显示会员权限
        $users_level_list = [];
        if ('on' == $arcrank) {
            $users_level_list = Db::name('users_level')->order('is_system desc, id asc')->column('level_name,id', 'id');
			$users_list = Db::name('users')->order('id asc')->column('nickname,litpic,id', 'id');
        }
        // 获取查询的表名
        $channeltype_info = $this->ChannelType->getInfo($channeltype);
        $controller_name = $channeltype_info['ctl_name'];
        $channeltype_table = $channeltype_info['table'];
        switch ($channeltype) {
            case '-1':
            {
                break;
            }
            default:
            {
                $list = array();
                $query_get = array();
                // 列表分页URL问号的查询部分
                $get_arr = input('get.');
                foreach ($get_arr as $key => $val) {
                    if (empty($val) || stristr($key, '/')) {
                        unset($get_arr[$key]);
                    }
                }
                $param_arr = input('param.');
                foreach ($param_arr as $key => $val) {
                    if (empty($val) || stristr($key, '/')) {
                        unset($param_arr[$key]);
                    }
                }
                $query_get = $get_arr;
                if(isset($query_get['t'])){
                   unset($query_get['t']);
                }
                $paginate = $query_get;
                $field = "b.*, a.*";
                $pages = Db::name('archives')
                    ->field($field)
                    ->alias('a')
                    ->join('arctype b', 'b.id = a.typeid', 'LEFT')
                    ->where($where_str)
                    ->orderRaw($orderby)
                    ->paginate($pagesize, false, $paginate);
                $list1 =  $pages->getCollection()->toArray();
                $aidArr = array();
                foreach ($list1 as $key => $val) {
                    // 获取指定路由模式下的URL
                    if ($val['is_part'] == 1) {
                        $val['typeurl'] = $val['typelink'];
                    } else {
                        $val['typeurl'] = typeurl($controller_name."/lists", $val);
                    }
                    // 文档链接
                    if ($val['is_jump'] == 1) {
                        $val['arcurl'] = $val['jumplinks'];
                    } else {
                        $val['arcurl'] = arcurl($controller_name.'/view', $val);
                    }
                    $val['litpic'] = get_default_pic($val['litpic']); // 默认封面图
                    // 属性控制是否使用缩略图
                    if(isset($thumb) && $thumb != ''){
                        if ('on' == $thumb) { 
                            $val['litpic'] = thumb_img($val['litpic']);
                        }
                    }
					
                    /*是否显示会员权限*/
                    !isset($val['level_name']) && $val['level_name'] = $val['arcrank'];
					!isset($val['users_name']) && $val['users_name'] = '';
					!isset($val['touxiang']) && $val['touxiang'] = get_default_pic();
                    if ('on' == $arcrank) {
                        if (!empty($users_level_list[$val['arcrank']])) {
                            $val['level_name'] = $users_level_list[$val['arcrank']]['level_name'];
                        } else if (empty($val['arcrank'])) {
                            $firstUserLevel = current($users_level_list);
                            $val['level_name'] = $firstUserLevel['level_name'];
                        }
                        if (!empty($users_list[$val['users_id']])) {
                            $val['users_name'] = $users_list[$val['users_id']]['nickname'];
							$val['touxiang'] = $users_list[$val['users_id']]['litpic'];
                        } else if (empty($val['users_id'])) {
                            $firstUserLevel = current($users_list);
                            $val['users_name'] = $firstUserLevel['users_name'];
							$val['touxiang'] = $users_list[$val['users_id']]['litpic'];
                        }
                    }
					
					
                    $list[$key] = $val;
                    array_push($aidArr, $val['aid']); // 文档ID数组
                }
                // 附加表
                if (!empty($addfields) && !empty($aidArr)) {
                    $addfields = str_replace('，', ',', $addfields); // 替换中文逗号
                    $addfields = trim($addfields, ',');
                    $addtableName = $channeltype_table.'_content';
                    $resultExt = Db::name($addtableName)->where('aid','IN',$aidArr)->column("aid,$addfields", 'aid');
                    // 自定义字段的数据格式处理
                    $resultExt = $this->fieldLogic->getChannelFieldList($resultExt, $channeltype, true);
                    foreach ($list as $key => $val) {
                        $valExt = !empty($resultExt[$val['aid']]) ? $resultExt[$val['aid']] : array();
                        $val = array_merge($valExt, $val);
                        $list[$key] = $val;
                    }
                }
                $result['pages'] = $pages; // 赋值分页输出
                $result['list'] = $list; // 赋值数据集
                break;
            }
        }
        return $result;
    }

    // 获取搜索分页列表
    public function getSearchList($pagesize = 10, $orderby = '', $addfields = '', $orderway = '', $thumb = '')
    {
        $result = false;
        empty($orderway) && $orderway = 'desc';
        $condition = array();
        // 获取到所有URL参数
        $param = input('param.');
        // 应用搜索条件
        foreach (['keywords','typeid','notypeid','channelid','flag','noflag'] as $key) {
            if (isset($param[$key]) && $param[$key] !== '') {
                if ('keywords' == $key) {
                    $keywords = trim($param[$key]);
                    $condition[] = ['a.title','=',$keywords];
                } else if ('typeid' == $key) {
                    $param[$key] = str_replace('，', ',', $param[$key]);
                    $param[$key] = preg_replace('/([^0-9,])/i', '', $param[$key]);
                    if (stristr($param[$key], ',')) { // 指定多个栏目ID
                        $typeids = explode(',', $param[$key]);
                    } else {
                        $search_type = input('param.type/s', 'default');
                        if ('default' == $search_type) { // 默认只检索指定的栏目ID，不涉及下级栏目
                            $typeids = [$param[$key]];
                        } else if ('sonself' == $search_type) { 
                            // 当前栏目以及下级栏目
                            $map = []; 
                            $map[] = ['id','=',$param[$key]];
                            $map[] = ['is_del','=',0];
                            $arctype_info = Db::name('arctype')->field('id,current_channel')->where($map)->find();
                            $childrenRow = $this->Arctype->getHasChildren($param[$key]);
                            foreach ($childrenRow as $k2 => $v2) {
                                if ($arctype_info['current_channel'] != $v2['current_channel']) {
                                    unset($childrenRow[$k2]); // 排除不是同一模型的栏目
                                }
                            }
                            $typeids = get_arr_column($childrenRow, 'id');
                        }
                    }
                    $condition[] = ['a.typeid','IN','{$typeids}'];
                } elseif ($key == 'channelid') {
                    $condition[] = ['a.channel','=','{$param[$key]}'];
                } elseif ($key == 'notypeid') {
                    $param[$key] = str_replace('，', ',', $param[$key]);
                    $param[$key] = preg_replace('/([^0-9,])/i', '', $param[$key]);
                    $notypeids = explode(',', $param[$key]);
                    $condition[] = ['a.typeid','<>','{$notypeids}'];
                } elseif ($key == 'flag') {
                    $flag_arr = explode(",", $param[$key]);
                    $where_or_flag = array();
                    foreach ($flag_arr as $k2 => $v2) {
                        if ($v2 == "c") {
                            array_push($where_or_flag, "a.is_recom = 1");
                        } elseif ($v2 == "h") {
                            array_push($where_or_flag, "a.is_head = 1");
                        } elseif ($v2 == "a") {
                            array_push($where_or_flag, "a.is_special = 1");
                        } elseif ($v2 == "j") {
                            array_push($where_or_flag, "a.is_jump = 1");
                        } elseif ($v2 == "p") {
                            array_push($where_or_flag, "a.is_litpic = 1");
                        } elseif ($v2 == "b") {
                            array_push($where_or_flag, "a.is_b = 1");
                        }
                    }
                    if (!empty($where_or_flag)) {
                        $where_flag_str = " (".implode(" OR ", $where_or_flag).") ";
                        array_push($condition, $where_flag_str);
                    } 
                } elseif ($key == 'noflag') {
                    $flag_arr = explode(",", $param[$key]);
                    $where_or_flag = array();
                    foreach ($flag_arr as $nk2 => $nv2) {
                        if ($nv2 == "c") {
                            array_push($where_or_flag, "a.is_recom <> 1");
                        } elseif ($nv2 == "h") {
                            array_push($where_or_flag, "a.is_head <> 1");
                        } elseif ($nv2 == "a") {
                            array_push($where_or_flag, "a.is_special <> 1");
                        } elseif ($nv2 == "j") {
                            array_push($where_or_flag, "a.is_jump <> 1");
                        } elseif ($nv2 == "p") {
                            array_push($where_or_flag, "a.is_litpic <> 1");
                        } elseif ($nv2 == "b") {
                            array_push($where_or_flag, "a.is_b <> 1");
                        }
                    }
                    if (!empty($where_or_flag)) {
                        $where_flag_str = " (".implode(" OR ", $where_or_flag).") ";
                        array_push($condition, $where_flag_str);
                    }
                } else {
                    $condition[] = ['a.{$key}','=','{$param[$key]}'];
                }
            }
        }
        $condition[] = ['a.arcrank','>',-1];
        $condition[] = ['a.status','=',1];
        $condition[] = ['a.is_del','=',0]; // 回收站功能
        $condition[] = ['a.channel','<>',6]; // 排除单页
        // 给排序字段加上表别名
        $orderby = getOrderBy($orderby,$orderway);
        // 数据查询，搜索出主键ID的值
        $list = array();
        $query_get = input('get.');
        if(isset($query_get['t'])){
           unset($query_get['t']);
        }
        $paginate = $query_get;
        $pages = Db::name('archives')
            ->field("a.aid")
            ->alias('a')
            ->join('arctype b', 'b.id = a.typeid', 'LEFT')
            ->where($condition)
            ->order($orderby)
            ->paginate($pagesize, false, $paginate);
        $list1 =  $pages->getCollection()->toArray();
        /**
         * 完善数据集信息
         * 在数据量大的情况下，经过优化的搜索逻辑，先搜索出主键ID，再通过ID将其他信息补充完整；
         */
        if ($list1) {
            $aids = get_arr_column($list1, 'aid');
            $fields = "b.*, a.*";
            $row = Db::name('archives')
                ->field($fields)
                ->alias('a')
                ->join('arctype b', 'a.typeid = b.id', 'LEFT')
                ->where('a.aid', 'IN', $aids)
                ->column($fields, 'aid');
            // 获取模型对应的控制器名称
            $channel_list = $this->ChannelType->getAll('id, ctl_name', array(), 'id');
            foreach ($list1 as $key => $val) {
                $arcval = $row[$val['aid']];
                $controller_name = $channel_list[$arcval['channel']]['ctl_name'];
                /*获取指定路由模式下的URL*/
                if ($arcval['is_part'] == 1) {
                    $arcval['typeurl'] = $arcval['typelink'];
                } else {
                    $arcval['typeurl'] = typeurl($controller_name."/lists", $arcval);
                }
                /*文档链接*/
                if ($arcval['is_jump'] == 1) {
                    $arcval['arcurl'] = $arcval['jumplinks'];
                } else {
                    $arcval['arcurl'] = arcurl($controller_name."/view", $arcval);
                }
                /*封面图*/
                $arcval['litpic'] = get_default_pic($arcval['litpic']); // 默认封面图
                if ('on' == $thumb) { // 属性控制是否使用缩略图
                    $arcval['litpic'] = thumb_img($arcval['litpic']);
                }
                $list[$key] = $arcval;
            }
            /*附加表*/
            if (!empty($addfields) && !empty($list)) {
                $channeltypeRow = $this->ChannelType->getAll('id,table', [], 'id'); // 模型对应数据表
                $channelGroupRow = group_same_key($list, 'current_channel'); // 模型下的文档集合
                foreach ($channelGroupRow as $channelid => $tmp_list) {
                    $addtableName = ''; // 附加字段的数据表名
                    $tmp_aid_arr = get_arr_column($tmp_list, 'aid');
                    $channeltype_table = $channeltypeRow[$channelid]['table']; // 每个模型对应的数据表
                    $addfields = str_replace('，', ',', $addfields); // 替换中文逗号
                    $addfields = trim($addfields, ',');
                    $addtableName = $channeltype_table.'_content';
                    $resultExt = Db::name($addtableName)->where('aid','IN',$tmp_aid_arr)->column("aid,$addfields", 'aid');
                    /*自定义字段的数据格式处理*/
                    $resultExt = $this->fieldLogic->getChannelFieldList($resultExt, $channelid, true);
                    foreach ($list as $key2 => $val2) {
                        $valExt = !empty($resultExt[$val2['aid']]) ? $resultExt[$val2['aid']] : array();
                        $val2 = array_merge($valExt, $val2);
                        $list[$key2] = $val2;
                    }
                }
            }
        }
        $result['pages'] = $pages; // 分页显示输出
        $result['list'] = $list; // 赋值数据集
        return $result;
    }

}