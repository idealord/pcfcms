<?php
/***********************************************************
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace think\template\taglib\gzpcf;

// 搜索表单
class TagSearchform extends Base
{
    //初始化
    protected function _initialize()
    {
        parent::_initialize();
    }

    // 获取搜索表单
    public function getSearchform($typeid = '', $channelid = '', $notypeid = '', $flag = '', $noflag = '', $type = '')
    {
        $searchurl = url('/Search/lists')->suffix('html')->domain(true)->build();
        $hidden = '';
        //静态打开
        if (2 == tpCache('seo.seo_pseudo')) {
            $hidden .= '<input type="hidden" name="c" value="Search" />';
            $hidden .= '<input type="hidden" name="a" value="lists" />';
        }
        // 手机端域名
        $goto = input('param.goto/s');
        $goto = trim($goto, '/');
        !empty($goto) && $hidden .= '<input type="hidden" name="goto" value="'.$goto.'" />';
        !empty($typeid) && $hidden .= '<input type="hidden" name="typeid" id="typeid" value="'.$typeid.'" />';
        !empty($channelid) && $hidden .= '<input type="hidden" name="channelid" id="channelid" value="'.$channelid.'" />';
        !empty($notypeid) && $hidden .= '<input type="hidden" name="notypeid" id="notypeid" value="'.$notypeid.'" />';
        !empty($flag) && $hidden .= '<input type="hidden" name="flag" id="flag" value="'.$flag.'" />';
        !empty($noflag) && $hidden .= '<input type="hidden" name="noflag" id="noflag" value="'.$noflag.'" />';
        !empty($type) && 'default' != $type && $hidden .= '<input type="hidden" name="type" id="type" value="'.$type.'" />';
        $result[0] = array(
            'searchurl' => $searchurl,
            'action' => $searchurl,
            'hidden' => $hidden,
        );
        return $result;
    }
}