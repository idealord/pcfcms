<?php
/**
 * 基础控制器
 * ============================================================================
 * 网站地址: http://www.pcfcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小潘 <1131680521@qq.com>
 * Date: 2019-12-21
 */
namespace app\api\controller;
use app\common\controller\Common;

class Base extends Common {

    // 初始化操作
    public function initialize() 
    {
        parent::initialize();
    }

}