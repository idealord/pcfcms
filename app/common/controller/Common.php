<?php
/***********************************************************
 * 公共控制器
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\common\controller;
use app\BaseController;
use think\facade\Session;
use think\facade\Db;
class Common extends BaseController 
{
    public $theme_style = '';
    public $view_suffix = 'html';
    public $gzpcf = array();
    public $globalTpCache = array();
    
    // 初始化操作
    public function initialize() 
    {
        parent::initialize();
        header("Cache-control: private");// history.back返回后输入框值丢失问题 
        //关闭网站
        if (empty(tpCache('web.web_status'))) {
            die("<!DOCTYPE HTML><html><head><title>网站暂时关闭</title><body><div style='text-align:center;font-size:20px;font-weight:bold;margin:50px 0px;'>网站暂时关闭，维护中……</div></body></html>");
        }
    }

}