<?php
/***********************************************************
 * 文档其它公用操作
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\controller;
use think\facade\Db;
use think\facade\Request;
use think\facade\Session;
use think\facade\Cache;
use app\admin\model\Custom as Custommodel;
use app\admin\model\Article as Articlemodel;

class Content extends Base
{
    public $popedom = '';
    public function initialize() 
    {
        parent::initialize();
        $ctl_act = Request::controller().'/index';
        $this->popedom = appfile_popedom($ctl_act);
    }

    public function index() 
    {
        $channel= input('param.channel/d', 0);//文档栏目id
        $typeid = input('param.typeid/d', 0); //文档分类栏目id
        $keywords = input('param.keywords', ''); //搜索
        $this->assign('channel', $channel);
        $this->assign('typeid', $typeid);
        $this->assign('keywords', $keywords);
        return $this->fetch();
    }
    
    // 左表数据
    public function leftmeun() 
    {
        if (Request::isAjax()) {
            $list = getmenucontent();
            $result = ['code' => 0, 'msg' => 'ok','count' =>count($list),'data' => $list];
            return $result;
        }
    }
 
    public function rightmeun() 
    {
        if (Request::isAjax()) {
            $post = input('param.');
            if($post['channel'] == 1){
                $pcfcmsmodel = new Articlemodel();
            }else{
                $pcfcmsmodel = new Custommodel();
            }
            return $pcfcmsmodel->tableData($post);
        }
    }

}