<?php
/***********************************************************
 * 广告管理
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\controller\channel;
use think\facade\Db;
use think\facade\Request;
use app\admin\controller\Base;
use app\admin\model\Ad as Admodel;
use app\admin\model\AdPosition as Adpositionmodel;

class Adposition extends Base
{

    public $popedom = '';
    public function initialize() {
        parent::initialize();
        $ctl_act = Request::controller().'/index';
        $this->popedom = appfile_popedom($ctl_act);
    }

    //列表
    public function index()
    { 
        //防止php超时
        function_exists('set_time_limit') && set_time_limit(0);
        //验证权限
        if(!$this->popedom["list"]){
            return $this->errorNotice(config('params.auth_msg.list'),true,3,false);
        }
        if (Request::isAjax()) {
            $Admodel = new Adpositionmodel();
            return $Admodel->tableData(input('param.'));
        }
        return $this->fetch();
    }

    //添加
    public function add()
    {
        //防止php超时
        function_exists('set_time_limit') && set_time_limit(0);
        if (Request::isPost()) {
            if(!$this->popedom["add"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.add')];
                    return $result;                    
                }
            }
            $Admodel = new Adpositionmodel();
            return $Admodel->toAdd(input('param.'));  
        }
        return $this->fetch();
    }

    //编辑
    public function edit()
    {
        //防止php超时
        function_exists('set_time_limit') && set_time_limit(0);
        if (Request::isPost()) {
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            }
            $Admodel = new Adpositionmodel();
            return $Admodel->toAdd(input('param.'));
        }
        $info = Db::name('ad_position')->where(['id' => input('param.id/d')])->find();
        if(!$info){$info = "";}
        $this->assign('info', $info);
        return $this->fetch();
    }

    //内容列表
    public function ad_index()
    { 
        //防止php超时
        function_exists('set_time_limit') && set_time_limit(0);
        //验证权限
        if(!$this->popedom["list"]){
            return $this->errorNotice(config('params.auth_msg.list'),true,3,false);
        }
        if (Request::isAjax()) {
            $Admodel = new Admodel();
            return $Admodel->tableData(input('param.'));
        }
        $id = input('param.id/d');
        $this->assign('id', $id);
        return $this->fetch();
    }

    //添加
    public function ad_add()
    {
        //防止php超时
        function_exists('set_time_limit') && set_time_limit(0);
        if (Request::isPost()) {
            if(!$this->popedom["add"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.add')];
                    return $result;                    
                }
            }
            $Ad_model = new Admodel();
            return $Ad_model->toAdd(input('param.'));  
        }
        $pid = input('param.id/d');
        $this->assign('pid', $pid);
        return $this->fetch();
    }

    //编辑
    public function ad_edit()
    {
        //防止php超时
        function_exists('set_time_limit') && set_time_limit(0);
        if (Request::isPost()) {
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            }
            $Ad_model = new Admodel();
            return $Ad_model->toAdd(input('param.'));
        }
        $info = Db::name('ad')->where('id', input('param.id/d'))->find();
        if($info){
            $info['start_time'] = date('Y-m-d h:i:s', $info['start_time']);
            $info['end_time'] = date('Y-m-d h:i:s', $info['end_time']);
        }else{
           $info = ""; 
        }
        $this->assign('info', $info);
        return $this->fetch();
    }

    // 删除单个
    public function del()
    {
        $ad = input('param.ad');
        if($ad){
            if (Request::isAjax()) {
                //验证权限
                if(!$this->popedom["delete"]){
                    if(config('params.auth_msg.test')){
                        $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                        return $result;
                    }else{
                        $result = ['status' => false, 'msg' => config('params.auth_msg.delete')];
                        return $result;                    
                    }
                } 
                $id = input('param.id/d');
                if (Db::name('ad')->where("id",$id)->delete()) {
                    $result = ['status' => true, 'msg' => '删除成功'];
                    return $result;
                } else {
                    $result = ['status' => false, 'msg' => '删除失败'];
                    return $result;
                }
                return $result;
            }  
        }else{
            if (Request::isAjax()) {
                //验证权限
                if(!$this->popedom["delete"]){
                    if(config('params.auth_msg.test')){
                        $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                        return $result;
                    }else{
                        $result = ['status' => false, 'msg' => config('params.auth_msg.delete')];
                        return $result;                    
                    }
                } 
                $id = input('param.id/d');
                $ad_count = Db::name('ad')->where('pid','=',$id)->count();
                if ($ad_count > 0){
                    $result = ['status' => false, 'msg' => '该位置下有广告，不允许删除，请先删除该位置下的广告'];
                    return $result;  
                }
                if (Db::name('ad_position')->where("id",$id)->delete()) {
                    $result = ['status' => true, 'msg' => '删除成功'];
                    return $result;
                } else {
                    $result = ['status' => false, 'msg' => '删除失败'];
                    return $result;
                }
                return $result;
            }             
        }
    }

    // 批量删除
    public function batch_del()
    {
        $ad = input('param.ad');
        if($ad){
            if (Request::isAjax()) {
                //验证权限
                if(!$this->popedom["delete"]){
                    if(config('params.auth_msg.test')){
                        $result = ['status' => 0, 'msg' => config('params.auth_msg.pcfcms')];
                        return $result;
                    }else{
                        $result = ['status' => 0, 'msg' => config('params.auth_msg.delete')];
                        return $result;                    
                    }
                } 
                $id_arr = input('param.del_id/a');
                $id_arr = eyIntval($id_arr);
                if(is_array($id_arr) && !empty($id_arr)){
                    foreach ($id_arr as $key => $val) {
                       Db::name('ad')->where('id',$val)->delete();
                    }
                    $result = ['code' => 1, 'msg' => '删除成功！'];
                    return $result;
                } else {
                    $result = ['code' => 0, 'msg' => '参数有误'];
                    return $result;
                }
            }
        }else{
            if (Request::isAjax()) {
                //验证权限
                if(!$this->popedom["delete"]){
                    if(config('params.auth_msg.test')){
                        $result = ['status' => 0, 'msg' => config('params.auth_msg.pcfcms')];
                        return $result;
                    }else{
                        $result = ['status' => 0, 'msg' => config('params.auth_msg.delete')];
                        return $result;                    
                    }
                } 
                $id_arr = input('param.del_id/a');
                $id_arr = eyIntval($id_arr);
                foreach ($id_arr as $k => $val) {
                    $ad_count = Db::name('ad')->where('pid',$val)->count();
                    if ($ad_count > 0){
                        $result = ['status' => false, 'msg' => '该位置下有广告，不允许删除，请先删除该位置下的广告'];
                        return $result;  
                    } 
                }
                if(is_array($id_arr) && !empty($id_arr)){
                    foreach ($id_arr as $key => $val) {
                       Db::name('ad_position')->where('id',$val)->delete();
                    }
                    $result = ['code' => 1, 'msg' => '删除成功！'];
                    return $result;
                } else {
                    $result = ['code' => 0, 'msg' => '参数有误'];
                    return $result;
                }
            }            
        }
    }

}
