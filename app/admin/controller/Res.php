<?php
/***********************************************************
 * 资源管理
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\controller;
use think\facade\Db;
use think\facade\Session;
use think\facade\Request;
use think\facade\Cache;
class Res extends Base
{
    public $popedom = '';
    public function initialize() {
        parent::initialize();
        $ctl_act = Request::controller().'/'.Request::action();
        $this->popedom = appfile_popedom($ctl_act);
    }

    public function index()
    {
        //验证查看权限
        if(!$this->popedom["list"]){
            return $this->errorNotice('您没有权限执行此操作',true,3,false);
        }
        if (Request::isAjax()) {
            $post = input('param.');
            if(isset($post['limit'])){
                $limit = $post['limit'];
            }else{
                $limit = 10;
            }
            $where = [];
            if (isset($post['name']) && $post['name'] != "") {
                $where[] = ['name', 'like', '%' . $post['name'] . '%'];
            }
            if (isset($post['id']) && $post['id'] != "") {
                $where[] = ['id', '=', $post['id']];
            }
            $tableWhere['where'] = $where;
            $tableWhere['order'] = ['ctime' => 'desc'];
            $list = Db::name('images')->where($tableWhere['where'])->order($tableWhere['order'])->paginate($limit);
            $newdata = $list->items();
            foreach ($newdata as $key => $value) {
                $newdata[$key]['id'] = $value['id'];
                $newdata[$key]['isdel'] = $value['isdel'];
                $newdata[$key]['name'] = $value['name'];
                $newdata[$key]['path'] = $value['path'];
                $newdata[$key]['type'] = $value['type'];
                $newdata[$key]['url'] = $value['url'];
                if(isset($value['ctime'])){
                   $newdata[$key]['ctime'] = pcftime($value['ctime']);
                }
            }
            $result = ['code' => 0, 'data' => $newdata,'count'=> $list->total()];
            return $result;
        }
        return $this->fetch();
    }

    // 删除单个
    public function del(){
         if (Request::isAjax()) {
            //验证权限
            if(!$this->popedom["delete"]){
                if(config('params.auth_msg.test')){
                    $result = ['code' => 0, 'msg' => config('params.auth_msg.pcfcms')];
                    return json($result);
                }else{
                    $result = ['code' => 0, 'msg' => config('params.auth_msg.delete')];
                    return json($result);                    
                }
            } 
            $post = input('param.');
            $image = Db::name('images')->where('id',$post['id'])->find();
            if ($image){
                $res = Db::name('images')->where("id",$post['id'])->delete();
                Db::name('common_pic')->where("pid",md5($image['path']))->delete();
                if ($image['type'] == 'local') {
                    if(!empty($image['path'])){
                        @unlink(ROOT_PATH."public".$image['path']);
                    } 
                }
                if ($res) {
                    $result = ['code' => 1, 'msg' => '删除成功'];
                    return json($result);
                }else{
                    $result = ['code' => 0, 'msg' => '删除失败'];
                    return json($result);
                }
            }
            return json($result);
        }       
    }

    // 批量删除
    public function batch_del(){
         if (Request::isAjax()) {
            //验证权限
            if(!$this->popedom["delete"]){
                if(config('params.auth_msg.test')){
                    $result = ['code' => 0, 'msg' => config('params.auth_msg.pcfcms')];
                    return json($result);
                }else{
                    $result = ['code' => 0, 'msg' => config('params.auth_msg.delete')];
                    return json($result);                    
                }
            } 
            $id_arr = input('del_id/a');
            $id_arr = eyIntval($id_arr);
            if(is_array($id_arr) && !empty($id_arr)){
                $res = delImage($id_arr);
                if ($res['code'] == 1) {
                    $result = ['code' => 1,'msg' => '删除成功'];
                }else{
                    $result = ['code' => 0, 'msg' => '参数有误'];
                }
                return json($result);
            } else {
                $result = ['code' => 0, 'msg' => '参数有误'];
                return json($result);
            }
        }       
    }

}