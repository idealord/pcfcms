<?php
/***********************************************************
 * 广告位置模型
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\model;
use think\facade\Db;
use think\facade\Request;
class AdPosition extends Common
{

    //列表
    public function tableData($post)
    {
        if(isset($post['limit'])){
            $limit = $post['limit'];
        }else{
            $limit = 10;
        }
        $tableWhere = $this->pcftableWhere($post);
        $list = Db::name('ad_position')->field($tableWhere['field'])->where($tableWhere['where'])->order($tableWhere['order'])->paginate($limit);
        $data = $this->tableFormat($list->items());
        foreach ($data as $key => $value) {
            $data[$key]['add_time'] = pcftime($value['add_time']);
        }
        $result = ['code' => 0, 'msg' => 'ok','count' =>$list->total(),'data' => $data];
        return $result;
    }

    protected function pcftableWhere($post)
    {
        $where = [];
        $result['where'] = $where;
        $result['field'] = "*";
        $result['order'] = "id desc";
        return $result;
    }

    //添加/编辑
    public function toAdd($data)
    {
        $result = array('status' => false,'data' => '','msg' => '','url' => '');
        $domain = Request::baseFile().'/channel.Adposition/index';
        //判断是新增还是修改
        if (isset($data['id']) && !empty($data['id'])) {
            $edit_data = array();
            $info = Db::name('ad_position')->where('id', $data['id'])->find();
            if (!$info) {
                $result = ['status' => false, 'msg' => '广告位置不存在'];
                return $result;
            }
            $where = [];
            $where[] = ['title','=',$data['title']];
            $where[] = ['id', '<>', $data['id']];
            $count = Db::name('ad_position')->where($where)->count();
            if($count > 0){
                $result = ['status' => false, 'msg' => '该标题已存在，请检查'];
                return $result;  
            }
            $edit_data['id'] = $data['id'];
            $edit_data['title'] = $data['title'];
            $edit_data['content'] = $data['content'];
            $edit_data['update_time'] = time();
            if (Db::name('ad_position')->where('id', $data['id'])->data($edit_data)->update()) {
                $result = ['status' => true, 'msg' => '修改成功','url' => $domain];
                return $result;
            } else {
                $result = ['status' => false, 'msg' => '修改失败'];
                return $result;
            }
        } else {
            $add_data = array();
            //判断标题是否重复
            $info = Db::name('ad_position')->where('title',$data['title'])->find();
            if ($data['title'] == $info['title']){
                $result = ['status' => false, 'msg' => '该标题已存在，请检查'];
                return $result;
            }
            $add_data['title'] = $data['title'];
            $add_data['content'] = $data['content'];
            $add_data['add_time'] = time();
            if (Db::name('ad_position')->save($add_data)) {
                $result = ['status' => true, 'msg' => '添加成功','url' => $domain];
                return $result;
            } else {
                $result = ['status' => false, 'msg' => '添加失败'];
                return $result;
            }
        }
    }

}
