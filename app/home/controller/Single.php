<?php
/***********************************************************
 * 单页模型 系统自带
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\home\controller;
use app\common\model\Arctype;
use think\facade\Db;
use think\facade\Request;
class Single extends Base
{
    // 模型标识
    public $nid = 'single';
    // 模型ID
    public $channeltype = '';

    public function _initialize() {
        parent::_initialize();
        $pcfglobal = get_global();
        $this->Arctypemodel = new Arctype();
        $channeltype_list = $pcfglobal['channeltype_list'];
        $this->channeltype = $channeltype_list[$this->nid];
    }

    public function lists($tid)
    {
        $tid_tmp = $tid;
        $seo_pseudo = tpCache('seo.seo_pseudo');
        $map = [];
        if (empty($tid)) {
            $map[] = ['channeltype','=',$this->channeltype];
            $map[] = ['parent_id','=',0];
            $map[] = ['is_hidden','=',0];
            $map[] = ['status','=',1];
            $res = Db::name('arctype')->where($map)->order('sort_order asc')->limit(1)->find();
            $typeurl = $this->Arctypemodel->getTypeUrl($res);
            header('Location: '.$typeurl);
        } else {
            if (3 == $seo_pseudo) {
                $map[] = ['dirname','=',$tid];
            } else {
                if (!is_numeric($tid) || strval(intval($tid)) !== strval($tid)) {
                    abort(404,'页面不存在');
                }
                $map[] = ['id','=',$tid];
            }
            $row = Db::name('arctype')->field('id,dirname')->where($map)->order('sort_order asc')->limit(1)->find();
            $tid = !empty($row['id']) ? intval($row['id']) : 0;
            $dirname = !empty($row['dirname']) ? $row['dirname'] : '';
            if (3 == $seo_pseudo) {
                $tid = $dirname;
            } else {
                $tid = $tid_tmp;
            }
        }
        $pcflist = new \app\home\controller\Lists($this->app);
        return $pcflist->index($tid);
    }
}